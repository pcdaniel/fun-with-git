
seaWaterTemp = [12,11,14,15,18,19,18,14,12,10,8,11]
month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

def make_print_str(temp,month):
    """Print the average temperature for a given month"""
    out_str = month+ " " + "#"*temp + " : " + str(temp)
    print(out_str)


def calculate_average(temps):
    n = len(temps)
    total = sum(temps)
    return total/n


def make_average_str(temps):
    mean = calculate_average(temps)
    print("\n")
    out_str = "Avg " + "#"*  int(mean) + " : " + str(mean)
    print(out_str)


if __name__ == "__main__":
    print("\n" +"-"*7+"Monthly Temperature"+"-"*7 + "\n")
    for m,t in zip(month,seaWaterTemp):
        make_print_str(t,m)

    make_average_str(seaWaterTemp)
    print("\n" +"-"*34 + "\n")
