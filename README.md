# git Demo/Game #

If you want to get some experience with git, signup for a gitlab account and email me your username. I will add you to the repo as a contributor.


---
1. __Clone this repository:__  
`git clone https://git.ucsc.edu/pcdaniel/fun-with-git.git`

2. __Make a Branch__  
`git checkout -b mybranch`

3. __Make some changes__  
Make a change to one of the files. For example you could leave a comment on your least-favorite superhero movie in [`git-is-cool.txt`](./git-is-cool.txt)
4. __Stage the changes__  
`git add --all` will look for changes in all of the files in this directory and subdirectory and stage them if there are any changes

5. __Commit the changes__  
`git commit -m "Add a very brief message here"` Leaving a message is important when you make a commit.

6. __Check that everything worked__  
`git status` will show what tracked files have been changed. Since we just committed, there shouldn't be any. This is a good way to make a sanity check.

7. __Push your Branch to gitlab__  
`git push origin mybranch` (or whatever your named your branch). Origin is used as shorthand for the url of the original repo that you cloned. If you made a repo on your computer before gitlab, you will need to add the remote url.

8. __Submit a Merge Request__  
Find your branch on gitlab ![](./images/branch.png)  

</br>  

__Create a Merge Request and leave a message for the reviewer.__ ![](./images/create-merge.png)

</br>  


__Accepting the Merge Request:__ A developer with the correct privileges (me in this case) is able to accept (or reject) the merge request. ![](./images/accept-merge.png)


__Congrats, you are now a contributor!!!__
